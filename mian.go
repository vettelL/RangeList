package main

import "fmt"

type RangeList struct {
	rightMap map[int]int
	leftArr  []int
}

func NewRangeList() *RangeList {
	this := &RangeList{
		rightMap: make(map[int]int),
		leftArr:  make([]int, 0),
	}
	return this
}

func (rangeList *RangeList) Get(index int) int {
	return rangeList.leftArr[index]
}

func (rangeList *RangeList) LowerBound(item int) int {
	i, j := 0, len(rangeList.leftArr)
	for i < j {
		h := i + (j-i)>>1
		if rangeList.leftArr[h] < item {
			i = h + 1
		} else {
			j = h
		}
	}
	return i
}

func (rangeList *RangeList) insertItem(item int) {
	i := rangeList.LowerBound(item)
	if i != len(rangeList.leftArr) {
		rangeList.leftArr = append(rangeList.leftArr, 0)
		copy(rangeList.leftArr[i+1:], rangeList.leftArr[i:])
		rangeList.leftArr[i] = item
	} else {
		rangeList.leftArr = append(rangeList.leftArr, item)
	}
}

func (rangeList *RangeList) removeItem(left, right int) {
	if right != len(rangeList.leftArr) {
		copy(rangeList.leftArr[left:], rangeList.leftArr[right:])
	}
	rangeList.leftArr = rangeList.leftArr[:len(rangeList.leftArr)-(right-left)]
}

func (rangeList *RangeList) find(left, right int) (int, int) {
	l, r := rangeList.LowerBound(left+1), rangeList.LowerBound(right+1)
	if l != 0 {
		l--
		if rangeList.rightMap[rangeList.Get(l)] < left {
			l++
		}
	}
	if l == r {
		return left, right
	}
	left = min(left, rangeList.Get(l))
	right = max(right, rangeList.rightMap[rangeList.Get(r-1)])
	for i := l; i < r; i++ {
		delete(rangeList.rightMap, rangeList.Get(i))
	}
	rangeList.removeItem(l, r)
	return left, right
}

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a <= b {
		return b
	}
	return a
}

// 时间复杂度：最坏O(n)，其中n为区间的个数, 用了二分做了一定的优化，最坏还是O(n)，但是最好复杂度可以到O(log n)
func (rangeList *RangeList) Add(rangeElement [2]int) error {
	l, r := rangeList.find(rangeElement[0], rangeElement[1])
	if _, ok := rangeList.rightMap[l]; !ok {
		rangeList.insertItem(l)
	}
	rangeList.rightMap[l] = r
	return nil
}

// 时间复杂度：最坏O(n)，其中n为区间的个数, 用了二分做了一定的优化，最坏还是O(n)，但是最好复杂度可以到O(log n)
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	l, r := rangeList.find(rangeElement[0], rangeElement[1])
	if rangeElement[0] > l {
		if _, ok := rangeList.rightMap[l]; !ok {
			rangeList.insertItem(l)
		}
		rangeList.rightMap[l] = rangeElement[0]
	}
	if rangeElement[1] < r {
		if _, ok := rangeList.rightMap[rangeElement[1]]; !ok {
			rangeList.insertItem(rangeElement[1])
		}
		rangeList.rightMap[rangeElement[1]] = r
	}
	return nil
}

// 时间复杂度：O(n) ，其中n为区间的个数
func (rangeList *RangeList) Print() error {
	for i, left := range rangeList.leftArr {
		if i != 0 {
			fmt.Print(" ")
		}
		fmt.Printf("[%d, %d)", left, rangeList.rightMap[left])
	}
	fmt.Println()
	return nil
}

func main() {
	rl := NewRangeList()
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
}
