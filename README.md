# RangeList

本次实现是维护一个有序的区间，这样的话不论是add还是remove、print操作，最坏时间复杂度都是O(n)，整体空间复杂度O(n),其中n为区间的个数

但如果是add、remove操作较多的情况，并且区间的数值范围较小的情况下
可以选择用线段树，这样add、remove的时间复杂度降低为O(log M), M为数值的范围
print操作时间复杂度为O(M)
整体的空间复杂度为O(M logM)
